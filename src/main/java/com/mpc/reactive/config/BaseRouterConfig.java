package com.mpc.reactive.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mpc.reactive.domain.Book;
import com.mpc.reactive.domain.BookQuery;
import com.mpc.reactive.domain.InMemoryDataSource;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.Validator;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuples;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicInteger;

import static com.mpc.reactive.helper.ReactiveControllerHelper.queryParamsToMono;
import static com.mpc.reactive.helper.ReactiveControllerHelper.requestBodyToMono;

/**
 * @author mipengcheng3
 * @Description 基础的controller配置类
 * @date Created in 10:55 2021/1/19
 */

@RequiredArgsConstructor
@Configuration
@Log4j2
public class BaseRouterConfig {
    /**
     * spring自带的验证器
     */
    private final Validator validator;
    /**
     * json转换器
     */
    private final ObjectMapper objectMapper;
    /**
     * uri前缀
     */
    private static final String PATH_PREFIX = "/routed/";
    /**
     * 记录重试次数
     */
    private AtomicInteger retryCount;

    @PostConstruct
    public void init() {
        retryCount = new AtomicInteger(0);
    }


    /**
     * 配置相应路由
     */
    @Bean
    public RouterFunction<ServerResponse> routers() {
        return RouterFunctions.route()
                .POST(PATH_PREFIX + "book", this::create)
                .GET(PATH_PREFIX + "books", this::findAll)
                .GET(PATH_PREFIX + "query-books", this::findByPage)
                .GET(PATH_PREFIX + "book/{isbn}", this::find)
                .PUT(PATH_PREFIX + "book/{isbn}", this::update)
                .DELETE(PATH_PREFIX + "book/{isbn}", this::delete)
                .POST(PATH_PREFIX + "retry", this::try3Times)
                .build();
    }


    /**
     * 分页查询
     */
    private Mono<ServerResponse> findByPage(ServerRequest request) {
        return queryParamsToMono(request, objectMapper, BookQuery.class, validator)
                .flatMap(query -> ServerResponse.ok().bodyValue(InMemoryDataSource.findBooksByQuery(query)));
    }


    /**
     * 删除书籍信息， 如果没有找到对应的数据，则返回 404 not found
     */
    private Mono<ServerResponse> delete(ServerRequest request) {
        String isbn = request.pathVariable("isbn");
        return InMemoryDataSource.findBookMonoById(isbn)
                .flatMap(book -> {
                    InMemoryDataSource.removeBook(book);
                    return ServerResponse.ok().build();
                }).switchIfEmpty(ServerResponse.notFound().build());
    }

    /**
     * 修改书籍信息，如果没有找到对应书籍，则返回404 not found
     */
    private Mono<ServerResponse> update(ServerRequest request) {
        String isbn = request.pathVariable("isbn");
        return InMemoryDataSource.findBookMonoById(isbn)
                .flatMap(book ->
                        requestBodyToMono(request, validator, Book.class)
                                .map(InMemoryDataSource::saveBook)
                                .flatMap(b -> ServerResponse.ok().build())
                                .switchIfEmpty(ServerResponse.notFound().build()));
    }


    /**
     * 查询书籍信息，如果没有找到相应的信息，则返回404 not found
     */
    private Mono<ServerResponse> find(ServerRequest request) {
        String isbn = request.pathVariable("isbn");
        return InMemoryDataSource.findBookMonoById(isbn)
                .flatMap(book -> ServerResponse.ok().bodyValue(book))
                .switchIfEmpty(ServerResponse.notFound().build());
    }

    /**
     * 查询所有的书籍信息
     */
    private Mono<ServerResponse> findAll(ServerRequest request) {
        Collection<Book> books = InMemoryDataSource.findAllBooks();
        return ServerResponse.ok().bodyValue(books);
    }

    /**
     * 添加书籍信息，如果数据已经存在则会报错
     */
    private Mono<ServerResponse> create(ServerRequest request) {
        return requestBodyToMono(request, validator, (t, errors) ->
                InMemoryDataSource.findBookMonoById(t.getIsbn())
                        .map(book -> {
                            errors.rejectValue("isbn", "already.exists", "Already exists");
                            return Tuples.of(book, errors);
                        }), Book.class)
                .map(InMemoryDataSource::saveBook)
                .flatMap(book -> ServerResponse.created(
                        UriComponentsBuilder.fromHttpRequest(request.exchange().getRequest())
                                .path("/").path(book.getIsbn()).build().toUri()
                ).build());
    }

    /**
     * 测试webClient重试请求
     */
    private Mono<ServerResponse> try3Times(ServerRequest request) {
        return requestBodyToMono(request, validator, (t, errors) ->
                InMemoryDataSource.findBookMonoById(t.getIsbn())
                        .map(book -> {
                            errors.rejectValue("isbn", "already.exists", "Already exists");
                            return Tuples.of(book, errors);
                        }), Book.class)
                .flatMap(book -> {
                    int count = retryCount.incrementAndGet();
                    if (count % 3 == 0) {
                        return ServerResponse.created(
                                UriComponentsBuilder.fromHttpRequest(request.exchange().getRequest())
                                        .path("/").path(book.getIsbn()).build().toUri()
                        ).build();
                    }
                    log.info("magic count is" + count);
                    return ServerResponse.status(500).build();
                });
    }

}
