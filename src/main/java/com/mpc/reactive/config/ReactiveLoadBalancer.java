package com.mpc.reactive.config;

import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.reactive.ReactorLoadBalancerExchangeFilterFunction;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * @author mipengcheng3
 * @Description
 * @date Created in 20:56 2021/3/2
 */

@Configuration
@EnableDiscoveryClient
public class ReactiveLoadBalancer {

    /**
     * 其实就是在原先的webclient上加一个过滤器，来进行负载均衡
     * */
    @Bean
    public WebClient.Builder loadBalancedWebClientBuilder(ReactorLoadBalancerExchangeFilterFunction filter) {
        return WebClient.builder().filter(filter).baseUrl("http://demo-client");
    }
}
