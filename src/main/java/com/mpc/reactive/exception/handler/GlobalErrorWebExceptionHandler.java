package com.mpc.reactive.exception.handler;

import com.mpc.reactive.helper.ReactiveControllerHelper;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.web.ResourceProperties;
import org.springframework.boot.autoconfigure.web.reactive.error.AbstractErrorWebExceptionHandler;
import org.springframework.boot.web.reactive.error.ErrorAttributes;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.Order;
import org.springframework.http.codec.ServerCodecConfigurer;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

/**
 * @author mipengcheng3
 * @Description 全局错误处理
 * @date Created in 9:59 2021/1/19
 */
@Component
@Order(-2)
public class GlobalErrorWebExceptionHandler extends AbstractErrorWebExceptionHandler {


    /**
     *
     * */
    public GlobalErrorWebExceptionHandler(ErrorAttributes errorAttributes,
                                          ResourceProperties resourceProperties,
                                          ApplicationContext applicationContext,
                                          ServerCodecConfigurer configurer) {
        super(errorAttributes, resourceProperties, applicationContext);
        this.setMessageWriters(configurer.getWriters());
    }

    @RequiredArgsConstructor
    @Data
    private static class Error {
        /**
         * 有错误的字段
         */
        final List<InvalidField> invalidFields;
        /**
         * 错误信息
         */
        final List<String> errors;
    }


    @RequiredArgsConstructor
    @Data
    private static class InvalidField {
        /**
         * 字段名称
         */
        final String name;
        /**
         * 错误信息
         */
        final String message;
    }

    @Override
    protected RouterFunction<ServerResponse> getRoutingFunction(ErrorAttributes errorAttributes) {
        return RouterFunctions.route(RequestPredicates.all(), serverRequest -> {
            Throwable throwable = errorAttributes.getError(serverRequest);
            if (throwable instanceof ReactiveControllerHelper.ValidationException) {
                return handleValidationException((ReactiveControllerHelper.ValidationException) throwable);
            }
            if (throwable instanceof ResponseStatusException) {
                return handleResponseStatusException((ResponseStatusException) throwable);
            }
            return ServerResponse.status(INTERNAL_SERVER_ERROR).build();
        });
    }

    /**
     * 处理服务器默认的状态码的错误信息
     */
    private Mono<ServerResponse> handleResponseStatusException(ResponseStatusException exception) {
        Error error = new Error(null, Arrays.asList(exception.getReason()));
        return ServerResponse.status(exception.getStatus()).bodyValue(error);
    }

    /**
     * 处理属性验证字段的错误
     */
    private Mono<ServerResponse> handleValidationException(ReactiveControllerHelper.ValidationException exception) {
        Errors errors = exception.getErrors();
        List<InvalidField> invalidFields = errors.getFieldErrors().stream()
                .map(error -> new InvalidField(error.getField(), error.getDefaultMessage()))
                .collect(Collectors.toList());
        List<String> theErrors = errors.getGlobalErrors().stream()
                .map(ObjectError::getDefaultMessage)
                .collect(Collectors.toList());
        Error error = new Error(invalidFields, theErrors);
        return ServerResponse.badRequest().bodyValue(error);
    }

}
