package com.mpc.reactive.helper;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.reactive.function.server.ServerRequest;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author mipengcheng3
 * @Description 用来协助处理controller中的额外需求
 * @date Created in 16:04 2021/1/18
 */


public class ReactiveControllerHelper {

    @RequiredArgsConstructor
    @Getter
    public static class ValidationException extends RuntimeException {
        /**
         * 抛出的异常信息
         */
        private final Errors errors;
    }

    /**
     * 外部的扩展validator，用来给用户一个自定义验证的机会
     */
    @FunctionalInterface
    public interface ExtraValidator<T> {
        /**
         * 对传入的类进行验证，这里的结果为响应式风格
         */
        Mono<Tuple2<T, Errors>> validate(T t, Errors errors);
    }



    /**
     * 使用提供的validator进行验证
     */
    public static <T> Mono<T> validate(Validator validator, Mono<T> mono) {
        return validate(validator, null, mono);
    }

    /**
     * 该方法进行验证，给开发者一个自定义验证的机会
     */
    public static <T> Mono<T> validate(Validator validator,
                                       @Nullable ExtraValidator<T> extraValidator,
                                       Mono<T> mono) {
        Assert.notNull(validator, "validator must not be null");
        Assert.notNull(mono, "mono must not be null");
        return mono.flatMap(
                t -> {
                    Errors errors = new BeanPropertyBindingResult(t, t.getClass().getName());
                    validator.validate(t, errors);
                    Mono<Tuple2<T, Errors>> aMono = Mono.empty();
                    if (null != extraValidator) {
                        aMono = extraValidator.validate(t, errors);
                    }
                    return aMono.switchIfEmpty(Mono.just(Tuples.of(t, errors)));
                }
        )
                .flatMap(tuple2 -> {
                    Errors errors = tuple2.getT2();
                    if (errors.hasErrors()) {
                        return Mono.error(new ValidationException(errors));
                    }
                    return Mono.just(tuple2.getT1());
                });
    }

    /**
     * 为request.bodyToMono增加验证逻辑，不能自定义验证器
     * */
    public static<T> Mono<T> requestBodyToMono(ServerRequest request, Validator validator, Class<T> cls) {
        return validate(validator, request.bodyToMono(cls));
    }


    /**
     * 为request.bodyToMono增加验证逻辑
     * */
    public static <T> Mono<T> requestBodyToMono(ServerRequest request, Validator validator,
                                                @Nullable ExtraValidator<T> extraValidator, Class<T> clz) {
        return validate(validator, extraValidator, request.bodyToMono(clz));
    }


    /**转换值，如果map并未真正的multiValueMap,这把值单独拿出来*/
    public static <T> T convertValue(ObjectMapper objectMapper,
                                     @Nullable MultiValueMap<String, String> map,
                                     Class<T> clz) {
        Assert.notNull(objectMapper, "objectMapper must not be null");
        Assert.notNull(clz, "clz must not be null");
        if (map == null) {
            return null;
        }
        Map<String, Object> theMap = new HashMap<>();
         map.entrySet().stream().map(
                e -> {
                    String key = e.getKey();
                    List<String> list = e.getValue();
                    if (list != null && list.size() == 1) {
                        return (new AbstractMap.SimpleEntry<>(key, list.get(0)));
                    }
                    return e;
                }
        ).forEach(e -> {
            theMap.put(e.getKey(), e.getValue());
         });
        return objectMapper.convertValue(theMap, clz);
    }

    /**如果request不使用json的话，获取所有的参数然后转化为mono*/
    public static <T> Mono<T> queryParamsToMono(ServerRequest request,
                                                ObjectMapper objectMapper,
                                                Class<T> clz,
                                                Validator validator) {
        return queryParamsToMono(request, objectMapper, clz, validator, null);
    }

    /**如果request不使用json的话，获取所有的参数然后转化为mono*/
    public static <T> Mono<T> queryParamsToMono(ServerRequest request,
                                                ObjectMapper objectMapper,
                                                Class<T> clz,
                                                Validator validator,
                                                @Nullable ExtraValidator<T> extraValidator) {
        Assert.notNull(request, "request must NOT be null");
        Assert.notNull(objectMapper, "objectMapper must NOT be null");
        Assert.notNull(clz, "clz must NOT be null");
        Assert.notNull(validator, "validator must NOT be null");

        Mono<T> mono = Mono.just(convertValue(objectMapper, request.queryParams(), clz));
        return validate(validator, extraValidator, mono);
    }
}
