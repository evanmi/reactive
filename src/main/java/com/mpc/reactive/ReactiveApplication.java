package com.mpc.reactive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * @author mipengcheng3
 * @Description 基础的controller配置类
 * @date Created in 10:55 2021/1/19
 */
@SpringBootApplication
public class ReactiveApplication {

    /***/
    public static void main(String[] args) {
        SpringApplication.run(ReactiveApplication.class, args);
    }

}
