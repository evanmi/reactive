package com.mpc.reactive.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

/**
 * @author mipengcheng3
 * @Description 书
 * @date Created in 15:31 2021/1/18
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Table("book")
public class Book {
    @Id
    @NotEmpty
    @Size(min = 3, max = 20)
    /**
     * 书的id
     * */
    private String isbn;

    @NotEmpty
    @Size(min = 3, max = 500)
    /**
     * 标题
     * */
    private String title;

    @NotNull
    @Min(0)
    /**
     * 价格
     * */
    private BigDecimal price;

    @NotEmpty
    @Size(min = 3, max = 50)
    /**
     * 类别
     * */
    private String category;
}
