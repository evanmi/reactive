package com.mpc.reactive.domain;


import org.springframework.util.StringUtils;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Comparator;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * @author mipengcheng3
 * @Description 模拟的内存数据库
 * @date Created in 15:43 2021/1/18
 */
public final class InMemoryDataSource {
    /***/
    public static final Book[] BOOKS = new Book[]{
            new Book("000001", "CS Book #1", BigDecimal.valueOf(19.99D), "CS"),
            new Book("000002", "CS Book #2", BigDecimal.valueOf(9.99D), "CS"),
            new Book("000003", "CS Book #3", BigDecimal.valueOf(39.99D), "CS"),

            new Book("000004", "Children Book #1", BigDecimal.valueOf(20.99D), "CHILDREN"),
            new Book("000005", "Children Book #2", BigDecimal.valueOf(25.99D), "CHILDREN"),
            new Book("000006", "Children Book #3", BigDecimal.valueOf(24.99D), "CHILDREN"),
            new Book("000007", "Children Book #4", BigDecimal.valueOf(10.99D), "CHILDREN"),

            new Book("000008", "Novel #1", BigDecimal.valueOf(6.99D), "NOVEL"),
            new Book("000009", "Novel #２", BigDecimal.valueOf(12.99D), "NOVEL"),
            new Book("000010", "Novel #３", BigDecimal.valueOf(8.99D), "NOVEL"),
            new Book("000011", "Novel #４", BigDecimal.valueOf(1.99D), "NOVEL")
    };

    /**用map模拟的数据库*/
    private static final Map<String, Book> BOOK_MAP = new ConcurrentHashMap<>();

    /**保存书籍信息到数据库中*/
    public static Book saveBook(Book book) {
        BOOK_MAP.put(book.getIsbn(), book);
        return book;
    }

    /**根据书籍的id来查询书籍信息*/
    public static Optional<Book> findBookById(String isbn) {
        return Optional.ofNullable(BOOK_MAP.get(isbn));
    }

    /**查询数据库中所有的书籍信息*/
    public static Collection<Book> findAllBooks() {
        return BOOK_MAP.values();
    }


    /**根据书籍的信息删除相应的数据信息*/
    public static void removeBook(Book book) {
        BOOK_MAP.remove(book.getIsbn());
    }

    /**根据id来查询数据信息，返回的mono的响应式结果*/
    public static Mono<Book> findBookMonoById(String isbn) {
        return Mono.justOrEmpty(findBookById(isbn));
    }

    /**根据example来过书籍信息*/
    public static Collection<Book> findBooksByQuery(BookQuery query) {
        return BOOK_MAP.values().stream()
                .filter(book -> {
                    boolean matched = true;
                    if (!StringUtils.hasText(query.getTitle())) {
                        matched &= book.getTitle().contains(query.getTitle());
                    }
                    if (query.getMinPrice() != null) {
                        matched &= (book.getPrice().compareTo(query.getMinPrice()) >= 0);
                    }
                    if (query.getMaxPrice() != null) {
                        matched &= (book.getPrice().compareTo(query.getMaxPrice()) <= 0);
                    }
                    return matched;
                })
                .sorted(Comparator.comparing(Book::getTitle))
                .skip((query.getPage() - 1) * query.getSize())
                .limit(query.getSize())
                .collect(Collectors.toList());
    }
}
