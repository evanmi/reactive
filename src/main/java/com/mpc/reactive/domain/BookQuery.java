package com.mpc.reactive.domain;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.math.BigDecimal;

/**
 * @author mipengcheng3
 * @Description 数据查询example类
 * @date Created in 15:45 2021/1/18
 */
@Data
public class BookQuery {
    /**标题*/
    private String title;
    /**最低价格*/
    private BigDecimal minPrice;
    /**最高价格*/
    private BigDecimal maxPrice;

    @Min(1)
    /**页数*/
    private int page = 1;

    @Min(0)
    @Max(500)
    /**每页的数量*/
    private int size = 10;
}