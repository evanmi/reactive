package com.mpc.reactive;

import com.mpc.reactive.domain.Book;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;

/**
 * @author mipengcheng3
 * @Description
 * @date Created in 20:29 2021/2/25
 */
public class WebClientR2dbcTest {

    private static Book firstBook;
    private static Book secondBook;
    private static WebClient webClient;

    @BeforeAll
    public static void initWebClient() {
        firstBook = Book.builder().isbn(String.valueOf(1))
                .category("TEST")
                .title("Book from webClient #1")
                .price(BigDecimal.valueOf(23.99))
                .build();

        secondBook = Book.builder().isbn(String.valueOf(2))
                .category("TEST")
                .title("Book from webClient #2")
                .price(BigDecimal.valueOf(55.99))
                .build();
        webClient = WebClient.create("http://localhost:8080/routed-r2dbc");
    }

    @Test
    public void testCreateMany() {
        webClient.post().uri("/books")
                .body(Flux.just(firstBook, secondBook), Book.class)
                .exchange()
                .doOnNext(
                        clientResponse -> System.out.println(">>>>>>>> POST RESPONSE STATUS CODE: " + clientResponse.statusCode())
                ).block();
    }


    @Test
    public void testGetABook() {
        webClient.get().uri("/book/{isbn}", firstBook.getIsbn())
                .retrieve()
                .bodyToMono(Book.class)
                .doOnNext(aBook -> System.out.println(">>>>>>> GET BOOK: " + aBook))
                .block();
    }

    @Test
    public void testUpdateABook() {
        firstBook.setPrice(BigDecimal.valueOf(39.99));
        webClient.put().uri("/book/{isbn}", firstBook.getIsbn())
                .body(Mono.just(firstBook), Book.class)
                .exchange()
                .doOnNext(
                        clientResponse -> System.out.println(">>>>>>>> PUT RESPONSE STATUS CODE: " + clientResponse.statusCode())
                ).block();
    }

    @Test
    public void testQueryManyBooks() {
        webClient.get().uri("/books")
                .retrieve()
                .bodyToFlux(Book.class)
                .doOnNext(aBook -> System.out.println(">>>>>>> GET BOOKS: " + aBook))
                .blockLast();
    }

    @Test
    public void testDeleteABook() {
        webClient.delete().uri("/book/{isbn}", firstBook.getIsbn())
                .exchange()
                .doOnNext(
                        clientResponse -> System.out.println(">>>>>>>> DELETE RESPONSE STATUS CODE: " + clientResponse.statusCode())
                ).block();
    }

}
