package com.mpc.reactive;

import com.mpc.reactive.domain.Book;
import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;

import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

/**
 * @author mipengcheng3
 * @Description
 * @date Created in 16:50 2021/2/9
 */
public class ReactiveClientTest {
    private static WebClient webClient;

    @BeforeAll
    public static void initWebClient() {
        webClient = WebClient.create("http://localhost:8080/routed");
    }

    @Test
    public void testAddBook() {
        Book aBook = Book.builder().isbn(String.valueOf(System.currentTimeMillis()))
                .category("TEST")
                .title("Book From WebClient")
                .price(BigDecimal.valueOf(23.99))
                .build();
        webClient.post().uri("/book")
                .body(Mono.just(aBook), Book.class)
                .exchange()
                .doOnNext(
                        clientResponse -> System.out.println(">>>>>>>> POST RESPONSE STATUS CODE: " + clientResponse.statusCode())
                )
                .block();
    }

    /**
     * 先创建书本信息后，使用创建的书本的isbn执行本测试
     */
    @Test
    public void getABook() {
        webClient.get().uri("/book/{isbn}", "1612862253876")
                .retrieve()
                .bodyToMono(Book.class)
                .doOnNext(aBook -> System.out.println(">>>>>>> GET BOOK: " + aBook))
                .block();
    }

    @Test
    public void updateABook() {
        Book originalBook = Book.builder().isbn(String.valueOf(System.currentTimeMillis()))
                .category("TEST")
                .title("Book From WebClient")
                .price(BigDecimal.valueOf(23.99))
                .build();
        webClient.post().uri("/book")
                .body(Mono.just(originalBook), Book.class)
                .exchange()
                .doOnNext(
                        clientResponse -> System.out.println(">>>>>>>> POST RESPONSE STATUS CODE: " + clientResponse.statusCode())
                )
                .block();
        originalBook.setPrice(BigDecimal.valueOf(39.99));
        webClient.put().uri("/book/{isbn}", originalBook.getIsbn())
                .body(Mono.just(originalBook), Book.class)
                .exchange()
                .doOnNext(
                        clientResponse -> System.out.println(">>>>>>>> PUT RESPONSE STATUS CODE: " + clientResponse.statusCode())
                )
                .block();
    }

    @Test
    public void findAllBooks() {
        webClient.get().uri("/books")
                .retrieve()
                .bodyToFlux(Book.class)
                .doOnNext((aBook -> System.out.println(">>>>>>> GET BOOKS: " + aBook)))
                .blockLast();
    }

    @Test
    public void testDeleteBook() {
        Book aBook = Book.builder().isbn(String.valueOf(System.currentTimeMillis()))
                .category("TEST")
                .title("Book From WebClient")
                .price(BigDecimal.valueOf(23.99))
                .build();
        webClient.post().uri("/book")
                .body(Mono.just(aBook), Book.class)
                .exchange()
                .doOnNext(
                        clientResponse -> System.out.println(">>>>>>>> POST RESPONSE STATUS CODE: " + clientResponse.statusCode())
                )
                .block();
        webClient.delete().uri("/book/{isbn}", aBook.getIsbn())
                .exchange()
                .doOnNext(
                        clientResponse -> System.out.println(">>>>>>>> DELETE RESPONSE STATUS CODE: " + clientResponse.statusCode())
                )
                .block();
    }

    @Test
    public void testRetries() {
        Book aBook = Book.builder().isbn(String.valueOf(System.currentTimeMillis()))
                .category("TEST")
                .title("Book From WebClient")
                .price(BigDecimal.valueOf(23.99))
                .build();
        webClient.post().uri("/retry")
                .body(Mono.just(aBook), Book.class)
                .exchange()
                .flatMap(clientResponse -> {
                    if (clientResponse.statusCode() != HttpStatus.CREATED) {
                        return clientResponse.createException().flatMap(Mono::error);
                    }
                    System.out.println(">>>>>>>> POST RESPONSE STATUS CODE: " + clientResponse.statusCode());
                    return Mono.just(clientResponse);
                })
                .retry(3)
                .block();

    }


    @Test
    public void testLimitTimes() {
        /**备注是的内容是高版本的改动，迭代太快，跟不上趟··*/
//        var httpClient = HttpClient.create()
//                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 500)
//                .doOnConnected(
//                        connection -> connection.addHandlerLast(new ReadTimeoutHandler(5, TimeUnit.SECONDS))
//                );
        HttpClient httpClient = HttpClient.create()
                .tcpConfiguration(
                        tcpClient -> {
                            tcpClient.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 500)
                                    .doOnConnected(
                                            connection -> connection.addHandlerLast(new ReadTimeoutHandler(5, TimeUnit.SECONDS))
                                    );
                            return tcpClient;
                        }
                );
        ReactorClientHttpConnector connector = new ReactorClientHttpConnector(httpClient);
        WebClient webClientWithHttpTimeout = WebClient.builder()
                .clientConnector(connector)
                .build();
        webClientWithHttpTimeout.get().uri("block5")
                .exchange()
                .doOnNext(
                        clientResponse -> System.out.println(">>>>>>>> DELETE RESPONSE STATUS CODE: " + clientResponse.statusCode())
                )
                .block();
    }
}
